import React from 'react';
import { FlatList, ActivityIndicator, Text, View, StyleSheet, RefreshControl  } from 'react-native';

export default class App extends React.Component {

  constructor(props){
    super(props);
    this.state ={ isLoading: true}
  }

  

  componentDidMount(){
    return fetch('https://planobgerencial.herokuapp.com/api/listaticketsandroid')
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson,
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }



  render(){

    if(this.state.isLoading){
      return(
        <View style={styles.container}>
          <ActivityIndicator/>
        </View>
      )
    }

    

    return(
      
      <View style={styles.container}>
      
        <FlatList
          data={this.state.dataSource}
          renderItem={({item}) => <Text>Codigo: {item.id}, {"\n"} Título: {item.titulo} {"\n"} {item.problema} {"\n"} Solução: {item.solucao} {"\n"} Nome do Cliente: {item.nome} {"\n"} </Text>}
          
          keyExtractor={({id}, index) => id}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 50
  },

  
});
  